
var start = true;
jQuery(document).ready(function(E)
	{
	keyboardeventKeyPolyfill.polyfill();

	var anim = false;
	function typed(finish_typing) {
		return function(term, message, delay, finish) {
			anim = true;
			var prompt = term.get_prompt();
			var c = 0;
			if (message.length > 0) {
				term.set_prompt('');
				var new_prompt = '';
				var interval = setInterval(function() {
					// handle html entities like &amp;
					var chr = $.terminal.substring(message, c, c+1);
					new_prompt += chr;
					term.set_prompt(new_prompt);
					c++;
					if (c == $.terminal.length(message)) {
						clearInterval(interval);
						// execute in next interval
						setTimeout(function() {
							// swap command with prompt
							finish_typing(term, message, prompt);
							anim = false
							finish && finish();
						}, delay);
					}
				}, delay);
			}
		};
	}

	var typed_message = typed(function(term, message, prompt) {
		term.set_command('');
		term.echo(message);
		term.set_prompt(prompt);
	});

    var N=navigator.userAgent.toLowerCase(), I=N.indexOf("android")>-1;

    E("#turbokiller").terminal(function(E, N) {
			console.log('wtf is n', N);
		function getIp(E) {

				$.ajax({jsonp: 'jsonp',
					dataType: 'jsonp',
					url: 'http://myexternalip.com/json',
					success: function(myip) {
						// N.echo("YOUR IP ADDRESS IS: " + myip + "\n")
						term.echo(" YOUR IP ADDRESS IS: " + myip + " \n");
						}
				});
		}

		E.match(/^\s*test\s*$/i)?
			N.clear() +
			typed_message(N, ' \n [[u;#fff;]test] &amp;&amp; \n \n NOT UNDERLINED &amp;&amp; \n \n [[u;#FFF;]UNDERLINED AGAIN] \n', 100):
		
		E.match(/^\s*help\s*$/i)?
			N.clear() +
			typed_message(N,
			" \n AVAILABLE COMMANDS: \n \n" +
			" HELP ....... SHOWS THIS SCREEN \n" +
			" ABOUT ...... INFORMATION ABOUT THE TURBOKILLER GAME \n" +
			" CONTACT .... SEND HATEMAIL TO THE DEVELOPERS \n" +
			" REDDIT ..... OPENS THE /R/TUBROKILLER SUBREDDIT \n" +
			" TWITTER .... THE OFFICIAL TURBOKILLER TWITTER FEED \n" +
			" GALLERY .... LIST OF TURBOKILLER IMAGE FILES - TYPE FILENAME TO OPEN \n" +
			" CLEAR ...... CLEARS THE SCREEN \n", 10):

		E.match(/^\s*date\s*$/i)?
		typed_message(N, (Date($.now())), 10):

		E.match(/^\s*ip\s*$/i)?getIp():

		E.match(/^\s*about\s*$/i)?
				N.clear() +
				typed_message(N,
				" \n =============================================================================== \n" +
				" \n" +
				"                                    ABOUT \n" +
				" \n" +
				" =============================================================================== \n \n" +
				" Twelve gunshots. A red-hooded corpse goes through the sixty-eighth floor's \n" +
				" plate-glass window into the lake below. Despite their best efforts, the body is \n" +
				" never found. \n \n" +
				" And then he wakes up; bruised, battered, and without his memory or identity -- \n" +
				" only a countdown timer burrowed deep in his skull, and a deep, seething desire \n" +
				" to finish what he started, no matter what the cost. \n" +
				" \n" +
				" Turbo Killer is a sidescroller shoot-em-up/bullet hell remix that gets \n" +
				" increasingly harder the longer you take. Equipped with only two pistols and the \n" +
				" ability to do superhuman flips, your goal is to be in and out as soon as \n" +
				" possible. \n" +
				" \n" +
				" * Branching, player-choice oriented story \n" +
				" * Brutal and unforgivingly difficult gameplay \n" +
				" * Twenty-one levels per playthrough \n" +
				" * Dangerously cheesy one-liners \n" +
				" * Guns, gore, noir, bullets, bloodshed -- what more could you possibly want? \n \n", 10):

		E.match(/^\s*contact\s*$/i)?
			N.clear() +
			typed_message(N,
				" \n =============================================================================== \n" +
				" \n" +
				"                                    CONTACT \n" +
				" \n" +
				" =============================================================================== \n" +
				" \n" +
				" Please send all fan-mail, fan art, questions, concerns, inquiries, \n" +
				" requests, hatemail, and brutal, unforgiving death threats to \n" +
				" NETRUNNERNOBODY@TURBOKILLER.NET \n \n \n" +
				" \n" +
				" Can you believe that someone snagged the .com before us?) \n" +
				" \n \n \n \n \n \n" +
				" (Please no actual death threats.) \n \n", 10):

		E.match(/^\s*reddit\s*$/i)?(window.open("https://reddit.com/r/turbokiller", "_blank"), N.clear() + N.echo("\n AUTHORIZING... \n ACCESS GRANTED. \n \n OPENING TURBOKILLER SUBREDDIT. \n")):

		E.match(/^\s*twitter\s*$/i)?(window.open("https://twitter.com/turbokillergame", "_blank"), N.clear() + N.echo("\n AUTHORIZING... \n ACCESS GRANTED. \n \n OPENING TURBOKILLER TWITTER FEED. \n")):

		/* E.match(/^\s*branch\s*$/i)?N.push(function(E, N) {
            E.match(/^\s*help\s*$/i)?
				typed_message(N, "\nTHIS IS A BRANCHING OPTION\nTYPE 'EXIT' TO LEAVE THE BRANCH", 10, function(){finished = true;}):
			E.match(/^\s*clear\s*$/i)?N.clear():
			E.match(/^\s*exit\s*$/i)?(1==N.level()&&N.token()||N.level()>1)&&N.pop():
			typed_message(N, "\nERROR: UNKNOWN LOG OR COMMAND\n", 10, function(){finished = true;})
        }
        , {	
            prompt: "BRANCH> ", name: "BRANCH"
        }): */

		E.match(/^\s*ping\s*$/i)?
			N.clear() + 
			typed_message(N, " \n PONG \n", 10):

		/* E.match(/^\s*blue\s*$/i)?
		N.echo("\nCHECK OUT THIS COLOR!\n", {
									finalize: function(div) {
										div.css("color", "blue");
										}
									}) +
		N.echo("NOW BACK TO NORMAL.\n"): */

		E.match(/^\s*gallery\s*$/i)?
		N.clear() +
		typed_message(N, " \n AUTHORIZING... \n ACCESS GRANTED. \n \n LISTING CONTENTS OF /TURBOKILLER/GALLERY/: \n \n IMAGE01.JPG \n IMAGE02.JPG \n IMAGE03.JPG \n", 10):
		
			E.match(/^\s*image01.jpg\s*$/i)?(window.open("files/image01.jpg", "_blank", "location=yes,height=500,width=500,scrollbars=yes,status=yes"), N.clear() + typed_message(N, " \n AUTHORIZING... \n ACCESS GRANTED. \n \n OPENING /TURBOKILLER/GALLERY/IMAGE01.JPG \n", 10)):
			E.match(/^\s*image02.jpg\s*$/i)?(window.open("files/image02.jpg", "_blank", "location=yes,height=500,width=500,scrollbars=yes,status=yes"), N.clear() + typed_message(N, " \n AUTHORIZING... \n ACCESS GRANTED. \n \n OPENING /TURBOKILLER/GALLERY/IMAGE02.JPG \n", 10)):
			E.match(/^\s*image03.jpg\s*$/i)?(window.open("files/image03.jpg", "_blank", "location=yes,height=500,width=500,scrollbars=yes,status=yes"), N.clear() + typed_message(N, " \n AUTHORIZING... \n ACCESS GRANTED. \n \n OPENING /TURBOKILLER/GALLERY/IMAGE03.JPG \n", 10)):

		E.match(/^\s*clear\s*$/i)?N.clear():

		E.match(/^\s*exit\s*$/i)?(1==N.level()&&N.token()||N.level()>1)&&N.pop():

		N.clear() + typed_message(N, " \n ERROR: UNKNOWN COMMAND \n", 10)
    }
    , {
        greetings: '', scrollOnEcho: true, scrollBottomOffset: 80, altinput:I, onBlur:function(){return!1}
    }
    ).css( {
        overflow: "auto"
    }
    )
		runInit();
		function runInit() {
			var term = E("#turbokiller").terminal();
			term.echo(" ================================================================================\n" +
							" \n" +
							"                             Welcome to the home of\n");
							term.echo(
							"    ______   _____________  _____       _   _______ _      _      ___________  \n" +
							"  |_   _| | | | ___ &#92; ___ &#92;|  _  |     | | / /_   _| |    | |    |  ___| ___ &#92; \n" +
							"    | | | | | | |_/ / |_/ /| | | |     | |/ /  | | | |    | |    | |__ | |_/ / \n" +
							"    | | | | | |    /| ___ &#92;| | | |     |    &#92;  | | | |    | |    |  __||    /  \n" +
							"    | | | |_| | |&#92; &#92;| |_/ /&#92; &#92;_/ /     | |&#92;  &#92;_| |_| |____| |____| |___| |&#92; &#92;  \n" +
							"    &#92;_/  &#92;___/&#92;_| &#92;_&#92;____/  &#92;___/      &#92;_| &#92;_/&#92;___/&#92;_____/&#92;_____/&#92;____/&#92;_| &#92;_| ", {finalize: function(div) {div.addClass("header");} });
							term.echo(" \n \n" +
							" ================================================================================ \n \n " +
							Date($.now()) + " \n \n" +
							" AVAILABLE COMMANDS: \n" +
							" HELP ....... SHOWS THIS SCREEN \n" +
							" ABOUT ...... INFORMATION ABOUT THE TURBOKILLER GAME \n" +
							" CONTACT .... SEND HATEMAIL TO THE DEVELOPERS \n" +
							" REDDIT ..... OPENS THE /R/TUBROKILLER SUBREDDIT \n" +
							" TWITTER .... THE OFFICIAL TURBOKILLER TWITTER FEED \n" +
							" GALLERY .... LIST OF TURBOKILLER IMAGE FILES - TYPE FILENAME TO OPEN \n" +
							" CLEAR ...... CLEARS THE SCREEN \n");
  		if(start) {
    		start = false;
  		}
	}
}

);
